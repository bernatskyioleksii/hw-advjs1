// Теоретичне питання
// 1. Поясніть своїми словами, як ви розумієте, як працює прототипне наслідування в Javascript
// Прототипи використовуються для спадкування властивостей і методів.

// 2. Для чого потрібно викликати super() у конструкторі класу-нащадка?
// Виклик super() дозволяє належним чином налаштувати інстанції класу-нащадка
//  з використанням спадкуваних властивостей та функціональності батьківського класу.

// Завдання
// 1. Створити клас Employee, у якому будуть такі характеристики - name (ім'я), age (вік), salary (зарплата). Зробіть так, щоб ці характеристики заповнювалися під час створення об'єкта.
// 2. Створіть гетери та сеттери для цих властивостей.
// 3. Створіть клас Programmer, який успадковуватиметься від класу Employee, і який матиме властивість lang (список мов).
// 4. Для класу Programmer перезапишіть гетер для властивості salary. Нехай він повертає властивість salary, помножену на 3.
// 5. Створіть кілька екземплярів об'єкта Programmer, виведіть їх у консоль.



class Employee {
	constructor(name, age, salary) {
	  this._name = name;
	  this._age = age;
	  this._salary = salary;
	}
 

  get name() {
	return this._name;
  }
  set name(newName){
	return this._name = newName
  }

  get age() {
	return this._age;
  }
  set age(newAge) {
	return this._age = newAge;
  }

  get salary(){
	return this._salary;
  }
  set salary(newSalary) {
	return this._salary = newSalary
  }
  
}


class Programmer extends Employee {
	constructor(name, age, salary, lang) {
		super(name, age, salary);
		this.lang = lang;
	}
	get salary() {
		return this._salary * 3;
	}
}

const programmer1 = new Programmer("John Doe", 30, 5000, ["JavaScript", "Python", "Java"]);
const programmer2 = new Programmer("Jane Smith", 25, 4000, ["C++", "Ruby"]);

console.log(programmer1.name); 
console.log(programmer1.age);      
console.log(programmer1.salary);     
console.log(programmer1.lang);       

console.log(programmer2.name); 
console.log(programmer2.age);     
console.log(programmer2.salary);    
console.log(programmer2.lang[0]); 
